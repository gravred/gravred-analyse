import pandas as pd
import numpy as np
from scipy.optimize import curve_fit
import scipy.signal as sg

import matplotlib.pyplot as plt


data = pd.read_csv("data\\psf_native.dat", sep='   ', dtype={'mA':np.float64, 'I':np.float64})

def psf(x, l, A, s):
    return  A * np.exp( - ((x-l)/s)**2 / 2 )

initials = [0, 1, 0.05]

params, errors = curve_fit(f=psf, xdata=data['mA'], ydata=data['I'], p0=initials)    #   Returs optimal parameters of gaussians and covariance matrix or stdevs for them.
errors = np.sqrt(np.diag(errors))


lin = np.linspace(min(data['mA']), max(data['mA']), 100)
cal = psf(lin, *params)

plt.plot(data['mA'], data['I'], 'x')

plt.plot(lin, cal)

plt.axvline(x=params[0]+params[2])
plt.axvline(x=params[0]-params[2], label='STDEV')

plt.axhline(y=params[1]/2, color='red', label='FWHM')

plt.show()


dl = (params[2]*2.3548, errors[2]*2.3548)
print(dl)

A = (6328 / dl[0],  dl[1]*6328/dl[0]**2)
print(A)


(630.14948-630.15008, 630.24857-630.24932)

(((1.01e-5)**2 + (6e-5)**2)**(1/2), ((1.16e-5)**2 + (6e-5)**2)**(1/2))