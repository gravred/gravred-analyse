import numpy as np

from Atlas import Spec_Atlas, c


def correctLines(spec, velo, times_o):
    times = {}
    
    for key in spec.keys():
        #\lambda_fit - \lambda_lab  - \lambda_dop - \lambda_kbv
        times[key] = times_o
        lambdaFits_byTime = spec[key]['fits']
        deltaLambdaDopplers_byTime = spec[key]['dops']
        lambdaModell = spec[key]['mod']
        lambdaLab = spec[key]['lab']
        deltaVeloDopplers_byTime = velo[key]['dops']

        # NOTE presumably correct
        deltaLambdaKonvekt = (lambdaModell[0]- lambdaLab[0],
                              np.sqrt(lambdaLab[1]**2 + lambdaModell[1]**2))

        
        invalids, extremes = [], []
        for i in range(len(lambdaFits_byTime[0])):
            if np.isnan(lambdaFits_byTime[0][i]) or np.isnan(lambdaFits_byTime[1][i]): invalids.append(i)
            
        #remove(invalids) 
        bla = invalids
        lambdaFits_byTime = (np.delete(lambdaFits_byTime[0], bla), np.delete(lambdaFits_byTime[1], bla))
        deltaLambdaDopplers_byTime = (np.delete(deltaLambdaDopplers_byTime[0], bla), np.delete(deltaLambdaDopplers_byTime[1], bla))
        deltaVeloDopplers_byTime = np.delete(deltaVeloDopplers_byTime, bla)
        times[key] = np.delete(times[key], bla)

        #Get indizees of extemes
        mean = np.mean(lambdaFits_byTime[0])
        stdev = np.std(lambdaFits_byTime[0])
        for i in range(len(lambdaFits_byTime[0])):
            if abs(lambdaFits_byTime[0][i]-mean) > 0.01 : extremes.append(i)

        #remove(extremes)
        bla = extremes
        lambdaFits_byTime = (np.delete(lambdaFits_byTime[0], bla), np.delete(lambdaFits_byTime[1], bla))
        deltaLambdaDopplers_byTime = (np.delete(deltaLambdaDopplers_byTime[0], bla), np.delete(deltaLambdaDopplers_byTime[1], bla))
        deltaVeloDopplers_byTime = np.delete(deltaVeloDopplers_byTime, bla)
        times[key] = np.delete(times[key], bla)
        
        
        print("For line  ", key, "  the  ", invalids, "  fitted and associated doppler data is discarderd because of nan data.")
        print("Therefrom for line  ", key, "  the  ", extremes, "  fitted and associated doppler data is discarderd because of farther than stdev. Consider improving your fittings.")
                
        deltaLambdaTotal_byTime = (lambdaFits_byTime[0] - lambdaLab[0], 
                    np.sqrt(lambdaFits_byTime[1]**2 + lambdaLab[1]**2))

        
        # NOTE inverse?    
        #lambdaFitsDopplerCorrected_byTime
        result_byTime = [lambdaFits_byTime[0]-deltaLambdaDopplers_byTime[0],
                         np.sqrt(lambdaFits_byTime[1]**2+deltaLambdaDopplers_byTime[1]**2)]

     
        # TODO: Korrektes Mitteln in stdevs?
        #result = (np.mean(result_byTime[0]), np.mean(result_byTime[1])/np.sqrt(len(result_byTime[1])))
        result = result_byTime

          
        # NOTE inverse?
        gravred = (result[0] - lambdaLab[0] - deltaLambdaKonvekt[0],
                   np.sqrt(result[1]**2 + lambdaLab[1]**2 + deltaLambdaKonvekt[1]**2))

        spec[key]['fits'] = lambdaFits_byTime
        spec[key]['dops'] = deltaLambdaDopplers_byTime
        spec[key]['kbv'] = deltaLambdaKonvekt
        spec[key]['gra'] = gravred

        velo[key]['fitsMLab'] = deltaLambdaToRelVel(*deltaLambdaTotal_byTime, *lambdaLab)
        velo[key]['dops'] = deltaVeloDopplers_byTime
        velo[key]['kbv'] = deltaLambdaToRelVel(*deltaLambdaKonvekt, *lambdaLab)
        velo[key]['gra'] = deltaLambdaToRelVel(*gravred, *lambdaLab)
        
    return spec, velo, times
        

# TODO: implement a conversion from nm to m/s
def deltaLambdaToRelVel(delta, std_delta, wavel, std_wavel):
    v = delta / wavel * c 
    std_v = np.sqrt((std_delta/wavel * c)**2 + (delta*std_wavel/(wavel**2) * c)**2)
    return (v, std_v)