import matplotlib.pyplot as plt


### Konfiguriere Orte der Mess- und Modelldaten
#path = "/home/sam/Downloads/Messdaten/2020-09-11/2020-09-11/"
path = "c:/EXPERIMENTE/gravred/2020-09-11/2020-09-11/"
#synthPath = "/home/sam/Downloads/Reduction/synth/"
synthPath = "c:/EXPERIMENTE/gravred/synth/"

#muss geändert werden, wenn die Daten von einem anderen Messtag und Zeit verwendet werden
imageTime = '20200911-100519'
darkTime = '20200911-063017'
flatTime = '20200911-114306'

borders = [900, 1800]
keys_relevant = ['sol_fe_2', 'sol_fe_3']
ref_keys = "atm_o2_1a, atm_o2_1b, atm_o2_2a, atm_o2_2b, atm_o2_3a, atm_o2_3b, atm_o2_4a, atm_o2_4b, atm_o2_5a, atm_o2_5b"
sun_keys = "sol_fe_1, unknown, sol_fe_2, sol_fe_3"

peak_order = 10
voigt_width = [0.6, 0.6]
gauss_width = 0.003/2*4

# Fitting initials for these are hardcoded in an obvious way in Synthing. These Keys are relevant for the rest of the evaluation.
synths = {'sol_fe_2':synthPath+'fe63015mu1.0_dgrd_vss.dat', 
          'sol_fe_3':synthPath+'fe63025mu1.0_dgrd_vss.dat'}

# True = Use checkpoints and skip calculus if valid, else set, False = definitly overwrite (if something has changed), None = no saving / loading
checkpoint = True   #NOTE: Caution when changing the dataset! Better firstly set it to False!


### Preparation of image Data through flat and dark corrections as well as mean calculation ### 
from imageSeries import *

timesPath = path + imageTime +".dat"
darkPath = path 
flatPath = path 

#Die Folgenden sind die Namen, unter denen es dann gespeichert wird:
avDarkName = darkTime +'_avDark.fits'
avFlatName = flatTime + '_avflat.fits'
avSpektrumName = imageTime + '_avGesamt.fits'
avFehlerName = imageTime + '_avErr.fits'

#Lade Darks und Flats ggf aus Speicher, kalkuliere Spektren und lade Zeiten
try: 
    if checkpoint is True: 
        save = True
        darkArray = loadImage(path + avDarkName, "Darks recovered.")
        flatArray = loadImage(path + avFlatName, "Flats recovered.")
        spektren = loadImage(path+avSpektrumName, "Spektren recovered.")
        fehler = loadImage(path+avFehlerName, "Fehler recovered.")
        try: 
            indizees = loadIndizees()
        except: 1/0
        print("Using checkpoints for image corrections.")
    else: 
        save = True if checkpoint is False else False
        1/0 # "temporal hook, avoiding introduction of a new temporal variable and nested ifs"
except ZeroDivisionError: 
    print("Redoing image corrections, because of missing or corrupted checkpoint-files.")
    darkArray = averageDarks(darkTime, darkPath, save=save) 
    flatArray = averageFlats(flatTime, flatPath, save=save) 
    spektren, fehler, indizees = applyCorrections(imageTime, path, darkArray=darkArray, flatArray=flatArray)

#show(spektren)

times = loadTimes(timesPath)
times = np.array([times[i] for i in indizees])


#Berechne den Durchschnitt aus N Zeilen des Spektrums
spektren, fehler, times = avaraging(spektren, fehler, times, N=25)
#show(spektren)



### Fitting ###
import Upsetting, Fitting, Synthing, Correcting, Evaluating

# Datenformatüberführung zwischen beiden Teilen
data, errs, times = spektren, fehler.reshape(fehler.shape[0]), times

# Parameter zur Konfiguration der Graphiken
show, absolute, compare, mean = [False, [0,-1], True], (False, False), (False, False), (True, True)

# Durchführen der Fits an die degradierten Simulations-Spektra
synths = Synthing.Synther(synths, gauss_width*10).getSynths(keys = keys_relevant, factor=0.1, checkpoint = False)

# Zuschneiden der Daten und konfigurieren der Linienschlüssel
refs, suns, slice = Upsetting.Upsetter().setup(show=show[0], datas=data, errs=errs, refs=ref_keys, suns=sun_keys, borders = borders)

checkpoint = False


# Durchführen der Spektrallinien-Fits und Wellenlängenkallibration
fittings = Fitting.Fitter(ref_keys=refs, sun_keys=suns, peak_order=peak_order, voigt_width=voigt_width,  gauss_width=gauss_width).fit(show=show[1], datas=data, errs=errs, slice=slice, compare=compare, checkpoint=checkpoint) 



### Calculation of Doppler ###
import Dopplering
from Atlas import Spec_Atlas


# refs = Spec_Atlas
dopplers, dopplers_vel = Dopplering.calculateDopplers(refs = synths, keys = keys_relevant, times=times, add=0, checkpoint=checkpoint)

### Nachverarbeitung der Messdaten
# transform an aggregate to new and better datastructure

data_spec = {key : {'fits':fittings[key], 'dops':dopplers[key], 'mod':synths[key], 'lab':Spec_Atlas[key], 'kbv':[], 'gra':[]} for key in keys_relevant}
data_velo = {key : {'fitsMLab':[], 'dops':dopplers_vel[key], 'kbv':[], 'gra':[]} for key in keys_relevant}
times = times

plt.plot(data_spec['sol_fe_2']['fits'][0])
plt.plot(data_spec['sol_fe_3']['fits'][0])
plt.show()

data_spec, data_velo, times = Correcting.correctLines(data_spec, data_velo, times)


from datetime import datetime 

"""
for key in keys_relevant: 
    zero = datetime.fromtimestamp(times[key][0])
    for i in range(len(times[key])):
        time = datetime.fromtimestamp(times[key][i])
        time = (time.hour-zero.hour)*60+(time.minute-zero.minute)+(time.second-zero.second)/60
        times[key][i] = time

"""
Evaluating.Evaluator(spec=data_spec, velo=data_velo, times=times).evaluate()

for k in keys_relevant:
    print(k, (np.mean(data_velo[k]['gra'][0]), np.mean(data_velo[k]['gra'][1])/np.sqrt(len(data_velo[k]['gra'][1]))), (np.std(data_velo[k]['gra'][0])))
    print(k, (np.mean(data_spec[k]['gra'][0]), np.mean(data_spec[k]['gra'][1])/np.sqrt(len(data_spec[k]['gra'][1]))), (np.std(data_spec[k]['gra'][0])))