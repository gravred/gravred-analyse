import matplotlib.pyplot as plt
from matplotlib.widgets import TextBox
import pylab

from Printing import Intendant
from Fitting import Fitter

class Upsetter():
    ''' Interface to select data-range, number spectral lines to be recognized on and index sepctral lines for referrence (wavelength solution). '''

    def setBorders(self, val, lr):
        ''' Set and update margins of data for later evaluation. Called by slider.on_changed. '''
        self.borders[lr] = val
        self.ax.set_xlim(self.borders)

    def submit(self, id):
        ''' Retrieve and save values from and finish plot window. Called by Button on click. '''

        try : refs, suns = self.box_refs.text, self.box_suns.text
        except: refs, suns = self.refs, self.suns

        
        for i in range(len(self.borders)):  self.borders[i] = int(self.borders[i])
        self.refs = [ref.strip() for ref in refs.split(',') if ref.strip()]
        self.suns = [sun.strip() for sun in suns.split(',') if sun.strip()]
		
        try: plt.close()
        except: pass


    def setup(self, show, datas, errs, refs='', suns='', borders = None):
        '''
        Starts setup window for setting values on visualized data.
        Parameter:
        ------
        data (np.array(int)): 1d array containing integer values (cleaned spectrum)
        '''       
        self.borders = borders                                                                    
        self.refs, self.suns = refs, suns

        data = datas[0]
        wid = len(data)
        if not self.borders:    self.borders = [0, wid]

        if show:
            intendant = Intendant()
            intendant.plot(data, title='Preparing Fits', label='Cleaned detector data', xlabel='Detector coordinate [pxl]', ylabel='Detector intensity []', show=False)
            self.ax = intendant.axe 
            self.ax.set_xlim(self.borders)

            plt.subplots_adjust(left=0.1, bottom=0.16)

            slider_left = pylab.Slider(plt.axes([0.1,0.02,0.325,0.02]),'Left cut [pixel]', 0, wid, valinit=self.borders[0],valstep=1)
            slider_right = pylab.Slider(plt.axes([0.55,0.02,0.325,0.02]),'Right cut [pixel]', 0, wid, valinit=self.borders[1],valstep=1)

            setBorders = [lambda val, lr = i: self.setBorders(val, lr) for i in [0,1]]

            slider_left.on_changed(setBorders[0])
            slider_right.on_changed(setBorders[1])        

            self.box_refs = TextBox(plt.axes([0.31,0.06,0.1,0.03]), 'List of line keys for spectral calibration (separeted by \',\') is ', initial=self.refs, color='1', hovercolor='0.9')
            self.box_suns = TextBox(plt.axes([0.8,0.06,0.1,0.03]), 'List of line keys for shift calculation is ', initial=self.suns, color='1', hovercolor='0.9')

            button_submit = pylab.Button(plt.axes([0.92, 0.194, 0.06, 0.65]), 'GO', color='0.075', hovercolor='#88bbff')  # Button dessen Aktion es ist, die Werte einzulesen, den Plot zu beenden und die Werte zurückzugeben
            button_submit.on_clicked(self.submit)

            intendant.show()
        
        else:  self.submit(None)

        i = -1 if self.borders[1] < self.borders[0] else 1
        slicer = slice(self.borders[0],self.borders[1],i)

        return self.refs, self.suns, slicer