'''
Global dictionary containig spectral lines. To be used as reference for wavelength solution or shift calculation. Floats in nanometers.
'''
#TODO Wellenlängen genauer herausfinden mit Fehlern!!! Nur die athmosphärischen sind letztlich von quantitativer Bedeutung. Die solaren nur zur Orientierung.
# Aus Pierce_Brekenridge_Linelist
Spec_Atlas = {
              'atm_o2_1a':  (629.21621  , 0.00005),
              'atm_o2_1b':  (629.29590  , 0.00010),

              'atm_o2_2a':  (629.51790  , 0.00010),
              'atm_o2_2b':  (629.59610  , 0.00010),
              
              'sol_fe_1' :  (629.78013  , 0.00020),
              
              'atm_o2_3a':  (629.84571  , 0.00005),
              'atm_o2_3b':  (629.92296  , 0.00020),

              'unknown'  :  (629.95957  , 0.00080),
              
              'sol_fe_2' :  (630.15008  , 0.00006), 
              'sol_fe_3' :  (630.24932  , 0.00006),

              'atm_o2_4a':  (630.20005  , 0.00005),
              'atm_o2_4b':  (630.27629  , 0.00010),

              'atm_o2_5a':  (630.58101  , 0.00005),
              'atm_o2_5b':  (630.65676  , 0.00030),
              }


# Lichtgeschwindigkeit in m/s
c = 299792458
