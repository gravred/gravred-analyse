import matplotlib.pyplot as plt
import numpy as np
from scipy import integrate, special
from scipy.optimize import curve_fit
from scipy.signal import argrelmax, argrelmin
import scipy.odr as sc_odr

from Printing import Spectral_Intendant, Wavelsol_Intendant, getColors
from Evaluating import Evaluator
from Atlas import Spec_Atlas
import Docstrings as DOC


class Fitter():

    def __init__(self, ref_keys, sun_keys, gauss_width=0.003/2, voigt_width=[0.9,0.9], peak_order=10):

        self.lineator = Lineator(ref_keys, sun_keys)
        self.intendant = None
        self.wavelsol_intendant = None

        self.gauss_width = gauss_width
        self.voigt_width = voigt_width
        self.peak_order = peak_order
        
        #self.colors = getColors([len(ref_keys), len(sun_keys)])
        self.colors = [['#7400b8', '#6930c3', '#5e60ce', '#5390d9', '#4ea8de', 
                        '#48bfe3', '#56cfe1', '#64dfdf', '#72efdd', '#80ffdb'],
                       ['#fff200', '#ffcc00', '#ffa600', '#ff8000'][::-1]]

        self.Athmos, self.Solars = [], []                                                           #   Lists to track evolution of detector drift (ref_pxls) and solar line drifts/oszilations (sun_wavs) over multiple measurements

    
    def fit(self, show, datas, errs, slice, compare, checkpoint = None):
        lines = {}
        try: 
            if checkpoint == True:
                checkpoint = False
                with open('checkpoints/fittings.npy', 'rb') as f:
                    def load(): return np.load(f)
                    for key in self.lineator.all_keys: lines[key] = (load(), load())
                print("Using checkpoints for fittings.")
                
                return lines
            else: 1/0
        except: print("Redoing fittings, because of missing or corrupted checkpoint-files.")

        #just for show
        for i in range(len(show)):  
            if show[i] < 0: show[i] += len(datas) 
            
        self.zero = slice.start
        self.errs = errs[slice]

        for i in range(len(datas)):
            self.data = datas[i,slice]
            self.maxs = self.findExtrema(True)                                            
        
            ref_peaks, sun_peaks = self.lineator.identify( self.findCores() )

            if i in show:   
                self.intendant = Spectral_Intendant()
            elif self.intendant: 
                self.intendant = None

            try:
                athmos = self.fitAthmos(ref_peaks)
                self.calib = Calibrator(athmos, self.lineator.ref_wavs, grad=3)        
                solars = self.calib.calc( self.fitSolars(sun_peaks), list=True )
            except Exception as e: 
                print('\n Fehler in Fitting, Spektrogramm %i! \n' % i,e)
                athmos = [(np.nan, np.nan) for ref in ref_peaks]
                solars = [(np.nan, np.nan) for sun in sun_peaks]
            else:
                if self.intendant: 
                    self.intendant.plot(self.data, self.errs, self.calib, slice.start, i, compare=compare)
                            
            self.Athmos.append(athmos)
            self.Solars.append(solars)

        lines = swapLinesDataStructure(self.lineator.sun_keys, self.lineator.ref_keys, self.Solars, self.Athmos)


        fits, fit_errs = [], []
        refs, ref_errs = [], []
        ress, res_errs = [], []
        for k in self.lineator.ref_keys:
            fits.append(lines[k][0][-1])
            fit_errs.append(lines[k][1][-1])
            refs.append(Spec_Atlas[k][0])
            ref_errs.append(Spec_Atlas[k][1])
            bla = self.calib.calc((lines[k][0][-1], lines[k][1][-1]))
            ress.append(bla[0]-Spec_Atlas[k][0])
            res_errs.append( np.sqrt(bla[1]**2 + Spec_Atlas[k][1]**2) )
        Wavelsol_Intendant().plot(refs, ref_errs, fits, fit_errs, self.calib, self.zero, len(self.data), ress, res_errs)


        if checkpoint == False:
            print("Saving checkpoints for fittings.")
            with open('checkpoints/fittings.npy', 'wb') as f:
                def save(a): np.save(f, a)
                for key in self.lineator.all_keys:
                    save(lines[key][0])
                    save(lines[key][1])
        
        return lines


    def findCores(self):
        #TODO: select peaks as most significant ones (maybe write a procedural routine by one self) -> increase saveness <- minimize resource load (repetitive task as everything here)
                                                       
        mins = self.findExtrema() #   Initial list of local minima in data to sort out from
        mins = [tpl[0] for tpl in sorted(mins, key=lambda x:x[1])]                                     # Sorted by peak height

        return mins[:len(self.lineator.range)]                                                                                      #   Initialize peaks list


    def findExtrema(self, up = False):
        method = argrelmax if up else argrelmin
        return [(ext, self.data[ext]) for ext in list(method(self.data, order=self.peak_order)[0])]


    def fitAthmos(self, peaks):
        
        profile = VoigtProfile(self)
        pxls = [self.fitLine(profile=profile, peak=peaks[i], color=self.colors[0][i], label='') for i in range(len(peaks))]

        return  pxls

 
    def fitSolars(self, peaks): 

        profile = GaussProfile(self)
        pxls = [self.fitLine(profile=profile, peak=peaks[i], color=self.colors[1][i], label='', comparable = True) for i in range(len(peaks))]

        return  pxls


    def fitLine(self, profile, peak, color=None, label='Absorbtion Line', comparable = False): #slice(0,-1)
        key, peak = peak[0], peak[1]
        slicer = profile.getSlice(peak)
        initials = profile.getInits(peak)
        bounds = profile.getBounds(slicer, *initials)
        
        params, errors = curve_fit(f=profile.calc, xdata=range(slicer.start, slicer.stop), ydata=self.data[slicer], sigma=self.errs[slicer], p0=initials, bounds=bounds, absolute_sigma=True,)    #   Returs optimal parameters of gaussians and covariance matrix or stdevs for them.

        slicer = profile.getSlice(int(params[0]))
        params, errors = curve_fit(f=profile.calc, xdata=range(slicer.start, slicer.stop), ydata=self.data[slicer], sigma=self.errs[slicer], p0=params, bounds=bounds, absolute_sigma=True,)    #   Returs optimal parameters of gaussians and covariance matrix or stdevs for them.

        errors = np.sqrt(np.diag(errors))                                                               #   Standart deviations are calculatet from covariance matrix.


        if self.intendant and color: self.intendant.addProfile(model=profile.calc, params=params, slice=slicer, color=color, label=label+key, compare=self.lineator.getWavel(key) if comparable else False) 


        return (params[0]+self.zero, errors[0])


    def getNears(self, peak):        
        # find a handy method to directly get element of list minimizing certain condition
        left, right = (0, self.data[0]), (len(self.data), self.data[-1])                                    #   Left and right margins for peak initialized
        for m in self.maxs:                                                                            #   For every local maximum identified:
            if m[0] < peak:                                                                  #   If maximum is left from peak center
                if m[0] > left[0]:    left = m                                                  #   And if maximum is right from current left margin (nearer to peak center) set to new left margin
            else:                                                                                   #   Else: if max is right from center
                if m[0] < right[0]:   right = m                                                 #   And nearer to center than current, set new
        return left, right
        


class Lineator():

    def __init__(self, ref_keys, sun_keys):
        self.ref_keys, self.sun_keys = ref_keys, sun_keys
        
        self.range = range(len(ref_keys) + len(sun_keys))

        self.all_wavs = {k: Spec_Atlas[k] for k in [*ref_keys,*sun_keys]}                                    #   Dict for all lines of use here, extracted from Global Atlas
        self.all_keys = [tpl[0] for tpl in sorted(self.all_wavs.items(), key=lambda x:x[1])]                  #   All line keys sorted by their coresponding wavelength

        self.ref_locs = [i for i in self.range if self.all_keys[i] in ref_keys]                     #   Positions of reference lines in list of all lines from left to right
        self.sun_locs = [i for i in self.range if self.all_keys[i] in sun_keys]                     #   Positions of sun lines in list of all lines from left to right
        
        sun_wavs = [self.all_wavs[k] for k in sun_keys]                                                      #   Wavelength of sun lines
        self.sun_wavs = [tpl for tpl in sorted(sun_wavs, key=lambda x:x[0])]                                 #   Sorted by height

        ref_wavs = [self.all_wavs[k] for k in ref_keys]                                                      #   Wavelength of ref lines
        self.ref_wavs = [tpl for tpl in sorted(ref_wavs, key=lambda x:x[0])]                                 #   Sorted by height


    def identify(self, all_peaks):
        all_peaks.sort()
        
        # sorting again maybe optional, dependend if for ... retains order
        ref_peaks = [(self.all_keys[i], all_peaks[i]) for i in self.ref_locs]                                                      #   Their pixel coordinates in provided data   (unsorted because of?)
        sun_peaks = [(self.all_keys[i], all_peaks[i]) for i in self.sun_locs]                                                      #   Their pixel coordinates in provided data   (unsorted because of?)

        return ref_peaks, sun_peaks

 #   def getKeys(self):  return [self.all_keys[i] for i in self.ref_locs], [self.all_keys[j] for j in self.sun_locs]

    def getWavel(self, key):    return  self.all_wavs[key]



class Calibrator():

    def __init__(self, pxls, wavs, grad=-1):

        pxls_errs = np.array([p[1] for p in pxls])                                                      #   Errors in pixel coordinates
        pxls = np.array([p[0] for p in pxls])                                                           #   Values

        wavs_errs = np.array([w[1] for w in wavs])                                                      #   Errors in pixel coordinates
        wavs = np.array([w[0] for w in wavs])      
        
        if grad < 0: grad=len(wavs)     
        if len(wavs) < 2 and grad > 1: grad = 1     
    
        model = sc_odr.Model(polynom)
        data = sc_odr.RealData(pxls, wavs, sx=pxls_errs, sy=wavs_errs)
        odr = sc_odr.ODR(data, model, beta0=list([0 for i in range(grad)]))
        out = odr.run()
        
        self.params = out.beta
        self.errors = out.sd_beta
        
        #self.params, errors = curve_fit(self.model, xdata=pxls, ydata=wavs, , absolute_sigma=True)#, bounds=bounds)# No bounds. May not work    #   Returs optimal parameters of gaussians and covariance matrix or stdevs for them.
        #self.errors = np.sqrt(np.diag(errors))                                                          #   Standart deviations are calculatet from covariance matrix.


    def calc(self, pxls, list=False):
        #   maybe be cool and let model return also error if param given
        if list: return [self.call(pxl) for pxl in pxls]
        else: return self.call(pxls)
        
    def call(self, pxl):
        if type(pxl) not in (list, tuple, range): return self.model(pxl)   
        else:   return (self.model(pxl[0]), self.modelError(pxl[0], pxl[1]))
        
    def model(self, x, *params):    
        return polynom(params if params else self.params, x)


    def modelError(self, x, dx):
        """Returns the gaussian error in spektral Dimension using wavelsolution with associated errors"""
        a, da = self.params, self.errors
        sigma, partdx = 0, 0

        # partial derivate for each paramter a
        for i in range(len(a)):
            sigma += (x**i * da[i])**2
        
        # partial derivate for x
        for i in range(1, len(a)):
            partdx += i * a[i] * x**(i-1)
            
        partdx *= dx
        sigma += partdx**2                
        
        sigma = sigma**(1/2)
        return sigma


def polynom(a, x):
    try:    f = np.full(len(x), 0, dtype=float)
    except TypeError: f = 0
    for i in range(len(a)): f += x**i * a[i]
    return f


class Profile():
    def __init__(self, fitter): self.fitter = fitter

class GaussProfile(Profile):
    def calc(self, x, l, c, A, s):
        return  c - A * np.exp( - ((x-l)/s)**2 / 2 )# / s / np.sqrt(2*np.pi)
    def getSlice(self, peak):
        # throug wavelsolution get pixel distance to both sides with self.gauss_width wavelength equivalent
        bords = [[peak, lambda x: x-1], [peak, lambda x: x+1]]
        peak = self.fitter.calib.calc(peak+self.fitter.zero)    # transalte peak pixel coordinate to spectral
        for bord in bords: 
            while abs(self.fitter.calib.calc(bord[0]+self.fitter.zero) - peak) < self.fitter.gauss_width: 
                bord[0] = bord[1](bord[0]) 
        start, stop = max([0, bords[0][0]]), min([bords[1][0]+1, len(self.fitter.data)])
        return slice(start+1, stop)
    def getInits(self, peak):
        nears = self.fitter.getNears(peak)
        back = (nears[0][1]+nears[1][1])//2
        return [peak, back, back-self.fitter.data[peak], abs(nears[0][0]-nears[1][0])//8]
    def getBounds(self, slice, l, c, A, s):
        l = [slice.start, slice.stop]
        c0 = c
        #aasdasdP
        c = [0, c0*10]
        A = [0, c0*10]
        s = [0, s*10e6+1]
        return tuple([p[i] for p in [l,c,A,s]] for i in [0,1])

class VoigtProfile(Profile):
    def __init__(self, fitter):
        super().__init__(fitter)
        self.sigma_to_gamma = 10
    def calc(self, x, *a):
        l, c, A, s, g = a[0], a[1], a[2], a[3], a[4]
        #return GaussProfile(self.fitter).calc(x, *[l,c,A,s])
        return c - A * special.voigt_profile(x-l, s, g)
    def getInits(self, peak):
        l, c, A, s = GaussProfile(self.fitter).getInits(peak)     
        g = l
        while A - self.fitter.data[g] > (A - self.fitter.data[l]) / 2: g-=1
        g = abs(g-l)
        return [l, c, A, s*self.sigma_to_gamma, g]
    def getSlice(self, peak):
        left, right = self.fitter.getNears(peak)
        #   stupid method to locate ~90% intensity borders of voigt profile
        #TODO something more intelligent
        drip, loc = self.fitter.data[peak], peak
        while type(left) is tuple:
            loc-=1
            if self.fitter.data[loc]-drip >= self.fitter.voigt_width[0]*(left[1]-drip): left = loc
        loc = peak
        while type(right) is tuple:
            loc+=1
            if self.fitter.data[loc]-drip >= self.fitter.voigt_width[1]*(right[1]-drip): right = loc
         
        start, stop = max([0, left]), min([right+1, len(self.fitter.data)])
        return slice(start+1, stop)
    def getBounds(self, slice, l, c, A, s, g):
        initials = GaussProfile(self.fitter).getBounds(slice, l, c, A, s)
        initials[0].append(0)
        initials[1].append(initials[1][3]*self.sigma_to_gamma)
        return initials



# prepare datastructure
# angeneommen suns/refs und solars/athmos sind zueinander konsistent in der reiehenfolge der linien, was implizit so sein sollte :{}
    
def swapLinesDataStructure(suns, refs, solars, athmos):
    lines = {key: [[],[]] for key in suns+refs}

    for keys, locs in [(suns, solars), (refs, athmos)]:
        for dataset in locs:
            for lindex in range(len(keys)):
                key = keys[lindex]
                
                lines[key][0].append(dataset[lindex][0])
                lines[key][1].append(dataset[lindex][1])

    lines = {key : (np.array(lines[key][0]), np.array(lines[key][1])) for key in suns+refs}
    return lines
