from datetime import datetime
import subprocess
import numpy as np
import pandas as pd

from Atlas import Spec_Atlas, c


#TODO: writing and reading
def calculateDopplers(refs, keys, times, add=0, checkpoint=None):
    """If checkpoint is True, try to read, else calculate. If checkpoint False Write, else not"""
    dopplers, dopplers_vel = {}, {}
    
    try: 
        if checkpoint == True:
            checkpoint = False
            with open('checkpoints/dopplers.npy', 'rb') as f:
                def load(): return np.load(f)
                for key in keys: 
                    dopplers[key] = (load(), load())
                    dopplers_vel[key] = load()
            print("Using checkpoints for dopplers.")
            return dopplers
        else: 1/0
    except: print("Redoing dopplers, because of missing or corrupted checkpoint-files.")

    dopplers = {key: [[],[]] for key in keys}
    dopplers_vel = {key: [] for key in keys}

    for timestamp, in times:
        """ 
        time = datetime.fromtimestamp(timestamp)

        # TODO: ADD correct command syntax
        #solarv -t -m SU90S -p -O IAG 2020-09-11Thh:mm:ss hpc 0 0    
        cmd = ['solarv', '-t', '-m', 'SU90S', '-O', 'IAG']
        cmd.append(str(time.year)+'-'+str(time.month)+'-'+str(time.day)+'T'+('%2i'%(time.hour+add))+':'+('%2i'%time.minute)+':'+('%2i'%time.second))
        cmd.append('hpc')
        cmd.append('0')
        cmd.append('0')

        # extraction from solarv's output. -> dopplershift in km/s
        result = subprocess.run(cmd, stdout=subprocess.PIPE).stdout.decode('utf-8')
        line = result.split('\n')[20]
        result = line.split(' ')
        result = list(filter(lambda a: a != '', result))[15]

        v = float(result)*1000
        """
        v=-500
            
        for key in keys:
            dopplers_vel[key].append(v)
            ref = refs[key]
            
            #deltaLambdaDoppler = (- ref[0] * (1 - np.sqrt((1+v/c)/(1-v/c))) , 
            #                        ref[1] * (1 - np.sqrt((1+v/c)/(1-v/c)))  )
            deltaLambdaDoppler = (   ref[0] * v/c,
                                     ref[1] * v/c)

            dopplers[key][0].append(deltaLambdaDoppler[0])
            dopplers[key][1].append(deltaLambdaDoppler[1])

    for key in dopplers.keys():
        dopplers[key] = (np.array(dopplers[key][0]), np.array(dopplers[key][1]))
        dopplers_vel[key] = np.array(dopplers_vel[key])

    if checkpoint == False:
        print("Saving checkpoints for dopplers.")
        with open('checkpoints/dopplers.npy', 'wb') as f:
            def save(a): np.save(f, a)
            for key in keys:
                save(dopplers[key][0])
                save(dopplers[key][1])
                save(dopplers_vel[key])
    
    return dopplers, dopplers_vel