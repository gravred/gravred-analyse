from Printing import Drift_Intendant, getColors
from Atlas import Spec_Atlas 
import numpy as np

class Evaluator():

    def __init__(self, spec, velo, times): self.spec, self.velo, self.times = spec, velo, times
    
    def evaluate(self):
        time_errs = {0 for k in self.times}

        intendant = Drift_Intendant()
        colors     = ['#ff6669', '#84dcc6']
        colors_kbv = ['#ffa6a9', '#b4fcf6']
        
        for key, color, kbv in zip(self.spec.keys(), colors, colors_kbv):
            line = self.velo[key]['fitsMLab']

            mean = (np.mean(line[0]), np.mean(line[1])/np.sqrt(len(line[0])))
            intendant.addDrift(key = key, coords = line[0], coord_errs = line[1], color=color, label='Gesamt '+str(key), mean=False, fmt='x--')

            intendant.addLine(y = self.velo[key]['kbv'][0], color=kbv, linestyle='-', label='Konvektiv ' + str(key), stdev=self.velo[key]['kbv'][1])
            
        key = list(self.spec.keys())[0]
        line = self.velo[key]['dops']
        errs = np.array([0 for i in line])
        intendant.addDrift(key = key, coords = line, coord_errs= errs, color='#d888cc', label='Doppler ', mean=False, fmt=':.')
            
        intendant.plot(self.times, time_errs, title='Gesamtverschiebung der Spektrallinien')

        #return False
    

        intendant = Drift_Intendant()
        
        for key, color in zip(self.spec.keys(), colors):
            line = self.velo[key]['gra']

            mean = (np.mean(line[0]), np.mean(line[1])/np.sqrt(len(line[0])))
            intendant.addDrift(key = key, coords = line[0], coord_errs= line[1], color=color, label=str(key), mean=mean, fmt='x--')
            
        intendant.plot(self.times, time_errs, title='Gravitative Rotverschiebung der Spektrallinien', ylabel='Gravitativ = Fit - Labor - Doppler - Konvektiv [m/s]')