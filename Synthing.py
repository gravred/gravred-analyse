import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit

import Fitting, Printing


class Synther:
    def __init__(self, synths, gauss_width):
        self.synths = synths
        self.gauss_width = gauss_width
        self.idata, self.ldata = None, None
    def getSynths(self, keys, factor=1, checkpoint = None):
        result = {}
        
        try: 
            if checkpoint == True:
                checkpoint = False
                with open('checkpoints/synthetics.npy', 'rb') as f:
                    def load(): return np.load(f)
                    for key in keys: result[key] = (load(), load())
                print("Using checkpoints for synthetics.")
                return result
            else: 1/0
        except: print("Redoing synthetics, because of missing or corrupted checkpoint-files.")
        
        profile = AlternateGaussProfile(self)

        hardcoded_peaks = [6301.4915, 6302.4820499999996]
        hardcoded_peaks_index = [25, 15]

        for key in self.synths.keys():
            intendant = Printing.Synth_Intendant()
            
            data = pd.read_csv(self.synths[key], sep='   ', dtype={'A':np.float64, 'I':np.float64}, engine = 'python')
            #TODO: faktor 0.1 for A!
            self.ldata = data['A']
            self.idata = data['I']

            # lambda location of peak
            peak_index = hardcoded_peaks_index.pop(0)
            peak_lambda = hardcoded_peaks.pop(0)
            
            slice = profile.getSlice(peak_index)
            initials = profile.getInits(peak_lambda)
            bounds = profile.getBounds(slice, *initials)
            data_errs = np.array([0.001 for i in range(len(self.idata))])
            
            params, errors = curve_fit(f=profile.calc, xdata=self.ldata[slice], ydata=self.idata[slice], p0=initials, bounds=bounds, sigma=data_errs[slice])    #   Returs optimal parameters of gaussians and covariance matrix or stdevs for them.
            errors = np.sqrt(np.diag(errors))   
               
            intendant.addProfile(model=profile.calc, params=params, slice=slice, label=key, compare=False)
            intendant.plot(self.idata, data_errs, self.ldata)
            intendant = None
            
            result[key] = [params[0]*factor, errors[0]*factor]
        
        if checkpoint == False:
            print("Saving checkpoints for synthetics.")
            with open('checkpoints/synthetics.npy', 'wb') as f:
                def save(a): np.save(f, a)
                for key in keys:
                    save(result[key][0])
                    save(result[key][1])

        print(result)
        return result

class AlternateGaussProfile(Fitting.GaussProfile):
    def __init__(self, synther): self.synther = synther
    def getSlice(self, peak):
        bords = [[peak, lambda x: x-1], [peak, lambda x: x+1]]
        for bord in bords: 
            while abs(self.synther.ldata[bord[0]] - self.synther.ldata[peak]) < self.synther.gauss_width: 
                bord[0] = bord[1](bord[0]) 
        start, stop = max([0, bords[0][0]]), min([bords[1][0]+1, len(self.synther.idata)])
        return slice(start, stop)
    def getInits(self, peak):
        nears = (0,len(self.synther.idata))
        back = max(self.synther.idata)
        return [peak, back, back-min(self.synther.idata), abs(nears[0]-nears[1])//8]
    def getBounds(self, slice, l, c, A, s):
        l = [min(self.synther.ldata), max(self.synther.ldata)]
        c0 = c
        #aasdasdP
        c = [0, c0*10]
        A = [0, c0*10]
        s = [0, s*10e2+1]
        return tuple([p[i] for p in [l,c,A,s]] for i in [0,1])
