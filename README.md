# Code for the group project intership at Max Planck Institute for Solar System Science

In the course of the project us five together with our supervisor at MPS in Göttingen built a spectrograph with resolution of (87 ± 4) · 10^3 in the size of a shoebox for just under 5000€. The project was funded by [Forschungsorientiertes Lehren und Lernen](https://www.uni-goettingen.de/en/420615.html) and Stufo. 
The aim was to measure the gravitational redshift of the sun - a very tiny lengthening of the Sun's light's wavelength due to the Sun's gravitational well. The effect has to separated from shifts due convection in the Sun's atmosphere, Earth's relative speed and rotation, as well as gravitational blueshift due to Earth's own gravitational well. 

The results are obtained roughly through: 
1. Mapping the sky's light spectrum using the built spectrograph, using [our steering software](https://gitlab.gwdg.de/gravred/gravred-control/-/tree/alt-beni?ref_type=heads)
2. extracting the spectral lines' positions using the code of this repository, and 
3. simulating and correcting for the other effects.

The final report in German language: [Final_Report__in_German_.pdf](https://gitlab.gwdg.de/gravred/gravred-analyse/-/blob/main/Final_Report__in_German_.pdf)

## Results

- The spectrograph achieved a resolution of (87 ± 4) · 10^3.

- We confirmed previous measurments and theoretical calculations of the gravitational to within 1 standard deviation. 

![Final result figure](Final_Result.png)

--- 

![Final poster](Final_Poster.png)
